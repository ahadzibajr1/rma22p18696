package ba.etf.rma22.projekat

import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.getAll
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.getDone
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.getFuture
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.getMyAnkete
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.getNotTaken
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.GrupaRepository.Companion.addUpisana
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.addUpisani
import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class AnketaRepositoryUnitTest {

    @After
    fun vrati() {
        GrupaRepository.vratiNaDefaultStanje()
        IstrazivanjeRepository.vratiNaDefaultStanje()
    }


   /* @Test
    fun getAllTest() {
        assertEquals(9,getAll().size)
    }*/


    @Test
    fun getMyAnketeTest() {

        var ankete = mutableListOf<Anketa>()
        ankete.addAll(getMyAnkete())
        assertEquals(5,ankete.size)
        addUpisani("Istraživanje broj 2",2)
        addUpisana("Grupa 3", "Istraživanje broj 2")
        ankete.clear()
        ankete.addAll(getMyAnkete())
        assertEquals(6,ankete.size)
        assertTrue(ankete.map { it.nazivGrupe }.contains("Grupa 3"))
        assertTrue(ankete.map { it.nazivIstrazivanja }.contains("Istraživanje broj 2"))

    }

    @Test
    fun getDoneTest() {

        var ankete = getDone().toMutableList()
        assertEquals(1,ankete.size)
        assertTrue(ankete.map { it.nazivGrupe }.contains("Grupa 1"))
        assertTrue(ankete.map { it.nazivIstrazivanja }.contains("Istraživanje broj 1"))

    }

    @Test
    fun getFutureTest() {
        var ankete = getFuture().toMutableList()

        assertEquals(2,ankete.size)
        assertTrue(ankete.map { it.nazivGrupe }.contains("Grupa 0"))
        assertTrue(ankete.map { it.nazivGrupe }.contains("Grupa 1"))
        assertTrue(ankete.map { it.nazivIstrazivanja }.contains("Istraživanje broj 1"))
        assertTrue(ankete.map { it.nazivIstrazivanja }.contains("Istraživanje broj 4"))
    }

    @Test
    fun getNotTakenTest() {
        var ankete = getNotTaken().toMutableList()

        assertEquals(1,ankete.size)

        assertTrue(ankete.map { it.nazivGrupe }.contains("Grupa 0"))
        assertTrue(ankete.map { it.nazivIstrazivanja }.contains("Istraživanje broj 4"))
    }



}