package ba.etf.rma22.projekat

import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.GrupaRepository.Companion.addUpisana
import ba.etf.rma22.projekat.data.repositories.GrupaRepository.Companion.getGroupsByIstrazivanje
import ba.etf.rma22.projekat.data.repositories.GrupaRepository.Companion.getUpisaneGrupeOld
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import org.junit.After
import org.junit.Test
import org.junit.Assert.*

class GrupaRepositoryUnitTest {

    @After
    fun vratiNaStaro() {
        GrupaRepository.vratiNaDefaultStanje()
        IstrazivanjeRepository.vratiNaDefaultStanje()
    }


    @Test
    fun getGroupsByIstrazivanjeTest() {
        assertEquals(2, getGroupsByIstrazivanje("Istraživanje broj 1").size)
        assertTrue(getGroupsByIstrazivanje("Istraživanje broj 1").map { it.naziv }.contains("Grupa 1"))
        assertFalse(getGroupsByIstrazivanje("Istraživanje broj 0").map { it.naziv }.contains("Grupa 2"))
        assertEquals(0, getGroupsByIstrazivanje("Test").size)
    }

    @Test
    fun getUpisaneGrupeTest() {

        assertEquals(2,getUpisaneGrupeOld().size)
        assertTrue(getUpisaneGrupeOld().map{it.naziv}.contains("Grupa 1"))
        assertTrue(getUpisaneGrupeOld().map{it.naziv}.contains("Grupa 0"))
    }

    @Test
    fun addUpisanaTest() {
        assertEquals(2,getUpisaneGrupeOld().size)
        addUpisana("Grupa 3", "Istraživanje broj 2")
        assertEquals(3,getUpisaneGrupeOld().size)
        assertTrue(getUpisaneGrupeOld().map{it.naziv}.contains("Grupa 3"))
    }



    @Test
    fun vratiNaDefaultTest() {

        assertEquals(2,getUpisaneGrupeOld().size)

        addUpisana("Grupa 3", "Istraživanje broj 2")

        GrupaRepository.vratiNaDefaultStanje()
        assertEquals(2,getUpisaneGrupeOld().size)
    }


}