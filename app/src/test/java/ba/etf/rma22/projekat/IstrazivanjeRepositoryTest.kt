package ba.etf.rma22.projekat

import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.addUpisani
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.getAll
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.getIstrazivanjeByGodina
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.getUpisani
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.vratiNaDefaultStanje
import org.junit.After
import org.junit.Test
import org.junit.Assert.*

class IstrazivanjeRepositoryUnitTest {

    @After
    fun vratiNaStaro() {
        IstrazivanjeRepository.vratiNaDefaultStanje()
    }

    @Test
    fun getUpisaniDefaultStanjeTest() {
        assertEquals(2,getUpisani().size)
        assertTrue(getUpisani().map { it.naziv }.contains("Istraživanje broj 1"))
        assertTrue(getUpisani().map { it.naziv }.contains("Istraživanje broj 4"))
    }

    @Test
    fun getUpisaniNakonDodavanjaTest() {
        addUpisani("Istraživanje broj 0", 1)

        assertEquals(3,getUpisani().size)
        assertTrue(getUpisani().map { it.naziv }.contains("Istraživanje broj 0"))
    }

    @Test
    fun getAllTest() {
        assertEquals(5,getAll().size)
    }

    @Test
    fun getIstrazivanjeByGodinaTest() {
        assertEquals(1, getIstrazivanjeByGodina(1).size)
        assertEquals(2, getIstrazivanjeByGodina(2).size)
        assertEquals(1, getIstrazivanjeByGodina(3).size)
        assertEquals(1, getIstrazivanjeByGodina(4).size)
        assertEquals(0, getIstrazivanjeByGodina(5).size)
        assertEquals(0, getIstrazivanjeByGodina(10).size)

        assertTrue(getIstrazivanjeByGodina(1).map{it.naziv}.contains("Istraživanje broj 0") )

    }


    @Test
    fun addUpisaniTest() {
        assertEquals(2, getUpisani().size)
        addUpisani("Istraživanje broj 3",3)

        assertEquals(3, getUpisani().size)

    }

    @Test
    fun vratiNaDefaultStanjeTest() {
        addUpisani("Istraživanje broj 3",3)

        assertEquals(3, getUpisani().size)

        vratiNaDefaultStanje()
        assertEquals(2, getUpisani().size)
    }

}