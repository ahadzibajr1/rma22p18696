package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.OdgovorRepository
import ba.etf.rma22.projekat.data.repositories.TakeAnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.reflect.KFunction3
import kotlin.reflect.KSuspendFunction1

class AnketaTakenViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun kreirajPokusaj(id: Int, pitanja: List<Pitanje>,anketa: Anketa, onSuccess: KFunction3<AnketaTaken, List<Pitanje>, Anketa, Unit>, onError: () ->Unit) {
        scope.launch {
            if(MainActivity.connected) {
                val res = TakeAnketaRepository.getPoceteAnkete()
                when (res) {
                    is List<AnketaTaken> -> {
                        var anketaTaken = res.find { it.AnketumId == id }
                        if (anketaTaken != null)
                            onSuccess?.invoke(anketaTaken, pitanja, anketa)
                        else {
                            val res2 = TakeAnketaRepository.zapocniAnketu(id)

                            when (res2) {
                                is AnketaTaken -> onSuccess?.invoke(res2, pitanja, anketa)
                                else -> onError?.invoke()
                            }
                        }
                    }
                    else -> {
                        val res2 = TakeAnketaRepository.zapocniAnketu(id)

                        when (res2) {
                            is AnketaTaken -> onSuccess?.invoke(res2, pitanja, anketa)
                            else -> onError?.invoke()
                        }
                    }
                }
            } else {
                val res = TakeAnketaRepository.getPoceteAnkete()
                if(res !is List<AnketaTaken> || res==null)
                    onError?.invoke()
                var anketaTaken = res!!.find { it.AnketumId == id }
                if (anketaTaken != null)
                    onSuccess?.invoke(anketaTaken, pitanja, anketa)
                else
                    onError?.invoke()
            }
        }
    }

    companion object {
        private val scope = CoroutineScope(Job() + Dispatchers.Main)
        fun getOdgovor(
            ktid: Int,
            pid: Int,
            onSuccess: KSuspendFunction1<Odgovor?, Unit>,
            onError: () -> Unit
        ) {
            scope.launch {

                    val res = OdgovorRepository.getOdgovoriAnketaTaken(ktid)
                    when (res) {
                        is List<Odgovor> -> {
                            var odg: Odgovor? = res.find { it.id == pid }
                            onSuccess?.invoke(odg)
                        }
                        else -> onError?.invoke()
                    }

            }
        }

        fun dodajOdgovor(ktid: Int, pid: Int,odg: Int, onSuccess:()->Unit, onError:()->Unit ) {
            scope.launch {
                if(MainActivity.connected) {
                    val res = OdgovorRepository.postaviOdgovorAnketa(ktid, pid, odg)

                    when (res) {
                        is Int -> onSuccess?.invoke()
                        else -> onError?.invoke()
                    }
                }
            }
        }

        fun dodajOdgovore(ktid:Int,odgovoriIPitanja: List<Pair<Int,Int>>,onSuccess:()->Unit, onError:()->Unit  ) {
            scope.launch {
                if (MainActivity.connected) {
                    for (o in odgovoriIPitanja) {
                        val res = OdgovorRepository.postaviOdgovorAnketa(ktid, o.first, o.second)

                        if (!(res is Int) || res == -1) {
                            onError?.invoke()
                            return@launch
                        }
                    }

                    onSuccess?.invoke()
                }
            }
        }

        fun dohvatiPokusaj(ktid: Int, onSuccess:(anketaTaken: AnketaTaken)->Unit, onError : ()->Unit) {
            scope.launch {

                val res = TakeAnketaRepository.getPoceteAnkete()
                when(res) {
                    is List<AnketaTaken> -> onSuccess?.invoke(res.find { it.id == ktid }!!)
                    else -> onError?.invoke()
                }

            }
        }
    }
}