package ba.etf.rma22.projekat


import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.data.repositories.AccountRepository.Companion.context
import ba.etf.rma22.projekat.view.*
import ba.etf.rma22.projekat.viewmodel.AccountViewModel
import ba.etf.rma22.projekat.viewmodel.ConnectivityBroadcastReceiver


class MainActivity : AppCompatActivity() {
    private val br: BroadcastReceiver = ConnectivityBroadcastReceiver()
    private val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager = findViewById(R.id.pager) as ViewPager2
        val fragments =  mutableListOf( FragmentAnkete(), FragmentIstrazivanje() )
        adapter = SimpleFragmentPagerAdapter(supportFragmentManager, fragments, lifecycle)
        viewPager.adapter = adapter

        applicationContext?.let{
            AccountRepository.context = it
        }

        viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                if (position == 0 && adapter.fragments[1] is FragmentPoruka) {
                    adapter.refreshFragment(1, FragmentIstrazivanje())
                }
                super.onPageSelected(position)
            }

        })



        val hash = intent?.getStringExtra("payload")
        if(hash != null)    {
            applicationContext?.let{
                AccountViewModel.postaviHash(hash,it,::onSuccess, ::onError)
            }
        }
    }



    override fun onResume() {
        super.onResume()
        registerReceiver(br, filter)
    }
    override fun onPause() {
        unregisterReceiver(br)
        super.onPause()
    }

    companion object {
        private lateinit var adapter: SimpleFragmentPagerAdapter
        private lateinit var viewPager: ViewPager2
        var connected: Boolean = true


        fun addPoruka(porukaFragment: Fragment) {
            adapter.refreshFragment(1,porukaFragment)
        }

        fun pokreniAnketu(fragmentiPitanja: MutableList<Fragment>) {
            adapter.replaceFragments(fragmentiPitanja)
            viewPager.setCurrentItem(0,false)
        }

        fun zaustaviAnketu() {
            adapter.replaceFragments(mutableListOf(FragmentAnkete(), FragmentIstrazivanje()))
            viewPager.setCurrentItem(0,false)
        }

        fun predajAnketu(fragmentPoruka: FragmentPoruka) {
            adapter.replaceFragments(mutableListOf(FragmentAnkete(), fragmentPoruka))
            viewPager.setCurrentItem(1,false)
        }

    }

    fun onSuccess() {
        Toast.makeText(context, "Successfully set up account", Toast.LENGTH_SHORT)

    }

    fun onError() {
        Toast.makeText(context, "Error setting up account", Toast.LENGTH_SHORT)
    }


}