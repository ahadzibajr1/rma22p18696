package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys=["GrupaId", "AnketaId"])
class GrupaAnketa (
    @ColumnInfo(name="GrupaId") var grupaId: Int,
    @ColumnInfo(name="AnketaId") var anketaId: Int,
    @ColumnInfo(name="IstrazivanjeId")  var istrazivanjeId: Int,
    @ColumnInfo(name="Grupa") var grupa: String,
    @ColumnInfo(name="Istrazivanje") var istrazivanje: String
) {

}