package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GrupaViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getGrupeByIstrazivanje(istrazivanjeID: Int, onSuccess: ( grupe: List<Grupa>, upisaneGrupe: List<Grupa>) -> Unit,
                                onError:() -> Unit) {
        scope.launch {
                val result1 = IstrazivanjeIGrupaRepository.getGrupeZaIstrazivanje(istrazivanjeID)
                val result2 = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
                when {
                    result1 is List<Grupa> && result2 is List<Grupa> -> onSuccess?.invoke(
                        result1,
                        result2
                    )
                    else -> onError?.invoke()
                }


        }
    }

    fun upisiStudentaUGrupu(grupaID: Int,grupa:String,istrazivanje:String,godina: Int, onSuccess:(g: String, i: String, god: Int) -> Unit, onError:()-> Unit) {
        scope.launch {
            if(MainActivity.connected) {
            val result = IstrazivanjeIGrupaRepository.upisiUGrupu(grupaID)

            if(result)
                onSuccess?.invoke(grupa,istrazivanje,godina)
            else  onError?.invoke()

        }
        }
    }
}