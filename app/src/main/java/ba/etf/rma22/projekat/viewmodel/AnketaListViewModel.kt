package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.reflect.KSuspendFunction2

class AnketaListViewModel(private val onSuccess: (ankete: List<Anketa>) -> Unit,
                          private val onError: () -> Unit) {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun dohvatiSveAnkete()
        {
        scope.launch{

                // Make the network call and suspend execution until it finishes
                val result = AnketaRepository.getAllAnkete()

                // Display result of the network request to the user
                when (result) {
                    is List<Anketa> -> onSuccess?.invoke(result.sortedBy { it.datumPocetak })
                    else -> onError?.invoke()
                }

        }
    }

    fun dohvatiMojeAnkete()
    {
        scope.launch{

                // Make the network call and suspend execution until it finishes
                val result = AnketaRepository.getUpisaneAnkete()

                // Display result of the network request to the user
                when (result) {
                    is List<Anketa> -> onSuccess?.invoke(result.sortedBy { it.datumPocetak })
                    else -> onError?.invoke()
                }

        }

    }

    fun dohvatiUrađeneAnkete()
    {
        scope.launch {

                // Make the network call and suspend execution until it finishes
                val result = AnketaRepository.getUrađene()

                // Display result of the network request to the user
                when (result) {
                    is List<Anketa> -> onSuccess?.invoke(result.sortedBy { it.datumPocetak })
                    else -> onError?.invoke()
                }

        }

    }

    fun dohvatiBudućeAnkete()
     {
        scope.launch{

                // Make the network call and suspend execution until it finishes
                val result = AnketaRepository.getBuduće()

                // Display result of the network request to the user
                when (result) {
                    is List<Anketa> -> onSuccess?.invoke(result.sortedBy { it.datumPocetak })
                    else -> onError?.invoke()
                }

        }
        //return AnketaRepository.getFuture()
    }

    fun dohvatiProšleAnkete() //: List<Anketa>
    {
        scope.launch {

                // Make the network call and suspend execution until it finishes
                val result = AnketaRepository.getProšle()

                // Display result of the network request to the user
                when (result) {
                    is List<Anketa> -> onSuccess?.invoke(result.sortedBy { it.datumPocetak })
                    else -> onError?.invoke()
                }

        }

    }

    fun provjeriMojaAnketa(anketa: Anketa, onSuccess: KSuspendFunction2<Boolean, Anketa, Unit>, onError: () -> Unit) {
        scope.launch {
                val result = AnketaRepository.checkMyAnketa(anketa)

                when (result) {
                    is Boolean -> onSuccess?.invoke(result, anketa)
                    else -> onError?.invoke()
                }

        }
    }
}