package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.GodinaRepository
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.data.sveGodine
import ba.etf.rma22.projekat.data.sveGrupe
import ba.etf.rma22.projekat.viewmodel.GrupaViewModel
import ba.etf.rma22.projekat.viewmodel.IstrazivanjeViewModel
import java.lang.Integer.parseInt

class FragmentIstrazivanje() : Fragment(){
    private lateinit var odabirGodina : Spinner
    private lateinit var odabirIstrazivanje : Spinner
    private lateinit var odabirGrupa : Spinner
    private lateinit var dugmeDodaj : Button
    private var istrazivanjaBackup: MutableList<Istrazivanje> = mutableListOf()
    private var istrazivanja : MutableList<String> = mutableListOf()
    private var grupe : MutableList<String> = mutableListOf()
    private var grupeBackup: MutableList<Grupa> = mutableListOf()
    private lateinit var adapterIstrazivanje : ArrayAdapter<String>
    private lateinit var adapterGrupe : ArrayAdapter<String>
    private var istrazivanjeViewModel: IstrazivanjeViewModel = IstrazivanjeViewModel()
    private var grupaViewModel: GrupaViewModel = GrupaViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.istrazivanje_fragment, container, false)

        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirIstrazivanje = view.findViewById(R.id.odabirIstrazivanja)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dugmeDodaj = view.findViewById(R.id.dodajIstrazivanjeDugme)

        odabirGodina.adapter = ArrayAdapter(requireActivity(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
            sveGodine().map{ it -> it.toString()})
        if (GodinaRepository.getPosljednjeOdabranaGodina() !=null)
            odabirGodina.setSelection(GodinaRepository.getPosljednjeOdabranaGodina()!!)

        adapterIstrazivanje = ArrayAdapter(requireActivity(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
            istrazivanja)

        odabirIstrazivanje.adapter = adapterIstrazivanje

        adapterGrupe = ArrayAdapter(requireActivity(),androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
            grupe)
        odabirGrupa.adapter = adapterGrupe

        odabirIstrazivanje.isEnabled = false
        odabirGrupa.isEnabled = false
        dugmeDodaj.isEnabled = false
        dugmeDodaj.isClickable = false

        odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @Override
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                addIstrazivanja(parseInt(odabirGodina.selectedItem.toString()))
            }
        }

        odabirIstrazivanje.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @Override
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                addGrupe(odabirIstrazivanje.selectedItem.toString())

            }
        }

        odabirGrupa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @Override
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                odabirIstrazivanje.isEnabled = true
                odabirGrupa.isEnabled = true
                if(!odabirGrupa.selectedItem.toString().isEmpty() && MainActivity.connected)
                    dugmeDodaj.isClickable = true
                    dugmeDodaj.isEnabled = true

            }
        }

        dugmeDodaj.setOnClickListener {
            if(odabirGrupa.selectedItem!=null && odabirIstrazivanje.selectedItem!=null && odabirGodina.selectedItem!=null) {

                var grupa = odabirGrupa.selectedItem.toString()
                var istrazivanje = odabirIstrazivanje.selectedItem.toString()
                var godina = odabirGodina.selectedItem.toString().toInt()

                if (grupa.isNotEmpty() && istrazivanje.isNotEmpty()) {
                    grupaViewModel.upisiStudentaUGrupu(
                        grupeBackup.find { it.naziv == grupa }!!.id,
                        grupa,
                        istrazivanje,
                        odabirGodina.selectedItemPosition,
                        ::onSuccessDodaj,
                        ::onError
                    )


                }
            }

        }

        return view
    }

    private fun addIstrazivanja(godina: Int) {
        istrazivanja.clear()
        adapterIstrazivanje.notifyDataSetChanged()
        adapterGrupe.notifyDataSetChanged()
        istrazivanjeViewModel.getIstrazivanjaByGodina(godina, ::onSuccessIstrazivanja, ::onError)

    }

    private fun addGrupe(istrazivanje: String) {
        if(istrazivanje.isEmpty())
            return
        grupe.clear()
        adapterGrupe.notifyDataSetChanged()
        var id = istrazivanjaBackup.find { it.naziv == istrazivanje }!!.id
        grupaViewModel.getGrupeByIstrazivanje(id, ::onSuccessGrupe, ::onError)
    }

    fun onSuccessIstrazivanja(istr: List<Istrazivanje>, grp: List<Grupa>) {
        istrazivanja.add("")
        istrazivanja.addAll(
            istr.filterNot { it.id in grp.map { it.istrazivanjeId } }.map { it.naziv })
        istrazivanjaBackup = istr.filterNot { it.id in grp.map { it.istrazivanjeId } }.toMutableList()
        adapterIstrazivanje.notifyDataSetChanged()
        adapterGrupe.notifyDataSetChanged()
        odabirIstrazivanje.setSelection(0);
        odabirIstrazivanje.isEnabled = true
        odabirGrupa.isEnabled = false
        dugmeDodaj.isClickable = false
        dugmeDodaj.isEnabled = false

    }

    fun onSuccessGrupe(grp: List<Grupa>, grpUpisane: List<Grupa>) {
        grupe.add("")
        grupe.addAll(grp.filterNot { it in grpUpisane }.map { it.naziv })
        grupeBackup = grp.filterNot { it in grpUpisane }.toMutableList()
        adapterGrupe.notifyDataSetChanged()
        odabirGrupa.setSelection(0);
        odabirIstrazivanje.isEnabled = true
        if(!odabirIstrazivanje.selectedItem.toString().isEmpty())
            odabirGrupa.isEnabled = true
        dugmeDodaj.isClickable = false
        dugmeDodaj.isEnabled = false

    }

    fun onSuccessDodaj(grupa: String, istrazivanje: String, godina: Int) {
        GodinaRepository.setPosljednjeOdabranaGodina(godina)
        val porukaFragment = FragmentPoruka("Uspješno ste upisani u grupu " + grupa + " istraživanja " + istrazivanje +"!")
        MainActivity.addPoruka(porukaFragment)
    }

    fun onError() {
        Toast.makeText(this.context, "Error", Toast.LENGTH_SHORT)
    }

    companion object {
        fun newInstance(): FragmentIstrazivanje = FragmentIstrazivanje()
    }
}