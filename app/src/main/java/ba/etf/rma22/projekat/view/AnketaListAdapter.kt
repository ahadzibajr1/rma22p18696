package ba.etf.rma22.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import java.text.SimpleDateFormat
import java.util.*


class AnketaListAdapter (
    private var ankete: List<Anketa>,
    private var onItemClicked: (anketa: Anketa) -> Unit
        ) : RecyclerView.Adapter<AnketaListAdapter.AnketaViewHolder>() {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): AnketaListAdapter.AnketaViewHolder {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.anketa_card, parent, false)
                return AnketaViewHolder(view)
            }
            override fun getItemCount(): Int = ankete.size
            override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
                holder.nazivAnkete.text = ankete[position].naziv;
                holder.nazivIstrazivanja.text = ankete[position].nazivIstrazivanja;
                holder.progres.progress = AnketaRepository.odrediProgres(ankete[position].progres!!)

                val datumIStatus : Pair<String,String> = AnketaRepository.odrediDatumIStatus(ankete[position])
                holder.datum.text = datumIStatus.first
                val bojaStatusa: String = datumIStatus.second
                val context: Context = holder.status.context
                var id: Int = context.resources
                    .getIdentifier(bojaStatusa, "drawable", context.packageName)
                holder.status.setImageResource(id)

                holder.itemView.setOnClickListener{onItemClicked(ankete[position])}

            }
            fun updateAnkete(ankete: List<Anketa>) {
                this.ankete = ankete
                notifyDataSetChanged()
            }
            inner class AnketaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val status: ImageView = itemView.findViewById(R.id.status)
                val nazivAnkete: TextView = itemView.findViewById(R.id.anketa)
                val nazivIstrazivanja: TextView = itemView.findViewById(R.id.istrazivanje)
                val datum: TextView = itemView.findViewById(R.id.datum)
                val progres: ProgressBar = itemView.findViewById(R.id.progresZavrsetka)
            }




        }