package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.sveGrupe
import ba.etf.rma22.projekat.data.upisaneGrupe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Long.parseLong

class GrupaRepository {
    companion object {
        private var upisaneGrupe: MutableList<Grupa>
        init {
            upisaneGrupe = mutableListOf<Grupa>()
            upisaneGrupe.addAll(upisaneGrupe())
        }
        fun getGroupsByIstrazivanje(nazivIstrazivanja: String): List<Grupa> {
            return sveGrupe().filter { it.nazivIstrazivanja.equals(nazivIstrazivanja) }
        }

        fun getUpisaneGrupeOld() : List<Grupa> {
            return upisaneGrupe
        }

        fun addUpisana(naziv : String, istrazivanje : String)  {

            upisaneGrupe.add(Grupa(0,naziv,null, istrazivanje))
        }

        fun vratiNaDefaultStanje() {
            upisaneGrupe.clear()
            upisaneGrupe.addAll(upisaneGrupe())
        }




    }
}