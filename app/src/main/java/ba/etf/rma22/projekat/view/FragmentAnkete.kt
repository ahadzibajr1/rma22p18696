package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.MainActivity.Companion.pokreniAnketu
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.AnketaTakenViewModel
import ba.etf.rma22.projekat.viewmodel.PitanjeViewModel
import java.util.*

class FragmentAnkete: Fragment() {
    private lateinit var ankete: RecyclerView
    private lateinit var filterAnketa : Spinner
    private lateinit var anketeAdapter: AnketaListAdapter
    private var anketaListViewModel =  AnketaListViewModel(::onSuccess, ::onError)
    private var pitanjeViewModel = PitanjeViewModel()
    private var anketaTakenViewModel = AnketaTakenViewModel()
    private var odabranaAnketa: Anketa? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      var view =  inflater.inflate(R.layout.anketa_fragment, container, false)
        ankete = view.findViewById(R.id.listaAnketa)
        ankete.layoutManager = GridLayoutManager(activity, 2)
        anketeAdapter = AnketaListAdapter(listOf()) { anketa -> showPitanja(anketa) }
        ankete.adapter = anketeAdapter
        anketaListViewModel.dohvatiMojeAnkete()

        filterAnketa = view.findViewById(R.id.filterAnketa)
        ArrayAdapter.createFromResource(requireActivity(),
            R.array.filteri,
            android.R.layout.simple_spinner_item).also {
                adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_list_item_1)
            filterAnketa.adapter = adapter
        }

        filterAnketa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @Override
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                filtrirajAnkete(filterAnketa.selectedItem.toString())
            }
        }

        return  view
    }
    companion object {
        var brojPitanja : Int = 0
        var brojPrethodnoOdgovorenih : Int = 0
        var odgovoriIPitanja : MutableList<Pair<Pitanje, Int?>> = mutableListOf()
        fun newInstance(): FragmentAnkete = FragmentAnkete()
    }

    override fun onResume() {
        filtrirajAnkete(filterAnketa.selectedItem.toString())
        super.onResume()
    }

    private fun filtrirajAnkete(kategorija : String) {

        when(kategorija) {
            "Sve moje ankete" -> {
                anketaListViewModel.dohvatiMojeAnkete()
            }
            "Sve ankete" -> {
                anketaListViewModel.dohvatiSveAnkete()
            }
            "Urađene ankete" -> {
                anketaListViewModel.dohvatiUrađeneAnkete()
            }
            "Buduće ankete" -> {
                anketaListViewModel.dohvatiBudućeAnkete()
            }
            "Prošle ankete" -> {
                anketaListViewModel.dohvatiProšleAnkete()
            }
        }
    }

    private fun showPitanja(anketa: Anketa) {
        if(anketa.datumPocetak.after(Calendar.getInstance().time))
            return

        anketaListViewModel.provjeriMojaAnketa(anketa,::onSuccessMojaAnketa,::onError)


    }

    fun onSuccess(ankete: List<Anketa>) {
        anketeAdapter.updateAnkete(ankete)
    }

    suspend fun onSuccessMojaAnketa(odg: Boolean, anketa: Anketa) {
        if(odg == false)
            return

        odabranaAnketa = anketa
        pitanjeViewModel.dohvatiPitanja(anketa.id, ::onSuccessPitanja, ::onError)

    }

    suspend fun onSuccessPitanja(pitanja: List<Pitanje>, id: Int) {
        if(pitanja.size==0) return
        anketaTakenViewModel.kreirajPokusaj(id, pitanja,odabranaAnketa!!, ::onSuccessPokusaj, ::onError)



    }

    fun onSuccessPokusaj(anketaTaken: AnketaTaken, pitanja: List<Pitanje>, anketa: Anketa) {
        val fragmenti : MutableList<Fragment> = mutableListOf()
        brojPitanja = pitanja.size
        brojPrethodnoOdgovorenih = 0
        odgovoriIPitanja.clear()
        for (pitanje in pitanja) {
            var temp = Pair(pitanje,null)
            odgovoriIPitanja.add(temp)
            fragmenti.add(FragmentPitanje(pitanje, anketaTaken, anketa))
        }

        fragmenti.add(FragmentPredaj(odabranaAnketa!!,anketaTaken.id))

        pokreniAnketu(fragmenti)
    }

    fun onError() {
        Toast.makeText(this.context, "Error", Toast.LENGTH_SHORT)
    }

}