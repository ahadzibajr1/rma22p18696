package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.odrediProgres
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.updateDatumRada
import ba.etf.rma22.projekat.viewmodel.AnketaTakenViewModel
import java.lang.Integer.parseInt
import java.util.*

class FragmentPredaj(
    val anketa: Anketa,
    val ktid: Int
    ): Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.predaj_fragment, container, false)


        val progresTekst = view.findViewById<TextView>(R.id.progresText)
        var progres = odrediProgres((FragmentAnkete.brojPrethodnoOdgovorenih + FragmentAnkete.odgovoriIPitanja.filter { it.second !=null }.size) /(FragmentAnkete.brojPitanja+0f))
        progresTekst.text = progres.toString() + "%"

        val dugmePredaj = view.findViewById<Button>(R.id.dugmePredaj)


        dugmePredaj.setOnClickListener { l ->
            //updateDatumRada(anketa)
            var odgovori = FragmentAnkete.odgovoriIPitanja.filter { it.second !=null }.map { Pair(it.first.id, it.second) }
            AnketaTakenViewModel.dodajOdgovore(ktid,
                odgovori as List<Pair<Int, Int>>, ::onSuccessPredaj, ::onError)

        }

        if((anketa.datumKraj!=null && anketa.datumKraj.before(Calendar.getInstance().time)) || anketa.progres >= 1 || MainActivity.connected==false) {
            dugmePredaj.isEnabled = false
            dugmePredaj.isClickable = false
        }
        return view
    }

    override fun onResume() {
        val progresTekst = requireView().findViewById<TextView>(R.id.progresText)
        var progres = odrediProgres((FragmentAnkete.brojPrethodnoOdgovorenih + FragmentAnkete.odgovoriIPitanja.filter { it.second !=null }.size) /(FragmentAnkete.brojPitanja+0f))
        progresTekst.text = progres.toString() + "%"
        super.onResume()
    }

    fun onSuccessPredaj() {
        val poruka: String = "Završili ste anketu " + anketa.naziv + " u okviru istraživanja " + anketa.nazivIstrazivanja
        MainActivity.predajAnketu(FragmentPoruka(poruka))
    }


    fun onError() {
        Toast.makeText(this.context, "Error", Toast.LENGTH_SHORT)
    }

    companion object {
        fun newInstance(anketa: Anketa, ktid: Int): FragmentPredaj = FragmentPredaj(anketa, ktid)
    }
}