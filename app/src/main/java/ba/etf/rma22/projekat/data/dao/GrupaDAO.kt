package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface GrupaDAO {

    @Query("SELECT * FROM grupa")
    suspend fun getAll() : List<Grupa>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(grupe: List<Grupa>)

    @Query("SELECT * FROM grupa WHERE id=:id")
    suspend fun getGrupa(id: Int) : Grupa

    @Query("SELECT * FROM grupa WHERE IstrazivanjeId=:istrazivanjeId")
    suspend fun getGrupeIstrazivanja(istrazivanjeId: Int) : List<Grupa>


}