package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Odgovor

@Dao
interface OdgovorDAO {

    @Query("SELECT * FROM odgovor")
    suspend fun getAll() : List<Odgovor>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(odgovori: List<Odgovor>)

    @Insert
    suspend fun insert(odgovor: Odgovor)

    @Query("SELECT * FROM odgovor WHERE AnketaTakenId=:atid")
    suspend fun getOdgovoriAnkete(atid: Int):List<Odgovor>
}