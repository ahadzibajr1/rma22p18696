package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken

@Dao
interface AnketaTakenDAO {

    @Query("SELECT * FROM anketataken")
    suspend fun getAll(): List<AnketaTaken>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(anketeTaken: List<AnketaTaken>)

    @Insert
    suspend fun insert(anketaTaken: AnketaTaken)

    @Query("UPDATE anketataken SET progres = :progres WHERE id = :id")
    suspend fun update(id: Int, progres: Int)
}