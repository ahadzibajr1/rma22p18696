package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Account

@Dao
interface AccountDAO {
    @Insert
    suspend fun insertAccount(vararg account: Account)

    @Delete
    suspend fun deleteAccounts(vararg account: Account)

    @Query("SELECT * FROM account")
    suspend fun getAccounts() : List<Account>
}