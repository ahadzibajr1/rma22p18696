package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.data.sveGodine

class GodinaRepository {
        companion object {
            private var posljednjeOdabranaGodina : MutableSet<Int>
            init {
                posljednjeOdabranaGodina = mutableSetOf()
            }
            fun getGodine(): List<String> {
                return sveGodine().map { it.toString() }
            }

            fun getPosljednjeOdabranaGodina() : Int? {
                return posljednjeOdabranaGodina.firstOrNull()
            }

            fun setPosljednjeOdabranaGodina(godina : Int) {
                posljednjeOdabranaGodina.clear()
                posljednjeOdabranaGodina.add(godina)
            }

        }
}