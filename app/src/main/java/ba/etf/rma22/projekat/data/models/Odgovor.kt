package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id", "AnketaTakenId"])
data class Odgovor(
    @ColumnInfo(name="odgovoreno") @SerializedName("odgovoreno") val odgovoreno: Int,
    @ColumnInfo(name="AnketaTakenId") @SerializedName("AnketaTakenId") val anketaTakenId: Int,
    @ColumnInfo(name="id") @SerializedName("PitanjeId") val id: Int
) {

}