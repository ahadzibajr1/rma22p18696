package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.svaIstrazivanja
import ba.etf.rma22.projekat.data.upisanaIstrazivanja
import ba.etf.rma22.projekat.data.upisaneGrupe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class IstrazivanjeRepository {
    companion object {
        private lateinit var upisanaIstrazivanja : MutableList<Istrazivanje>
        init {
            upisanaIstrazivanja = mutableListOf()
            upisanaIstrazivanja.addAll(upisanaIstrazivanja())
        }
        fun getIstrazivanjeByGodina(godina: Int): List<Istrazivanje> {
            return svaIstrazivanja().filter { it.godina == godina }
        }

        fun getAll(): List<Istrazivanje> {
            return svaIstrazivanja()
        }

        fun getUpisani(): List<Istrazivanje> {
            return upisanaIstrazivanja
        }

        fun addUpisani(naziv : String, godina: Int) {
            upisanaIstrazivanja.add(Istrazivanje(0,naziv, godina))
        }

        fun vratiNaDefaultStanje() {
            upisanaIstrazivanja.clear()
            upisanaIstrazivanja.addAll(upisanaIstrazivanja())
        }


    }
}
