package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.*

class IstrazivanjeViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getIstrazivanjaByGodina(godina: Int, onSuccess: (istrazivanja: List<Istrazivanje>, grupe: List<Grupa>) -> Unit,
                onError:() -> Unit) {
        scope.launch{
                val result1 = IstrazivanjeIGrupaRepository.getIstrazivanjaByGodina(godina)
                val result2 = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
                when {
                    result1 is List<Istrazivanje> && result2 is List<Grupa> -> onSuccess?.invoke(
                        result1,
                        result2
                    )
                    else -> onError?.invoke()
                }


        }
    }
}