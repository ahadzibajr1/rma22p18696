package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.GrupaStudent

@Dao
interface GrupaStudentDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(gs : GrupaStudent)

    @Query("SELECT * FROM GrupaStudent")
    suspend fun getAll() : List<GrupaStudent>
}