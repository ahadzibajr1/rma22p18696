package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import java.util.*

fun svaIstrazivanja() : List<Istrazivanje> {

    return listOf(
        Istrazivanje(0,"Istraživanje broj 0", 1),
        Istrazivanje(1,"Istraživanje broj 1", 2),
        Istrazivanje(2,"Istraživanje broj 2", 2),
        Istrazivanje(3,"Istraživanje broj 3", 3),
        Istrazivanje(4,"Istraživanje broj 4", 4)
    )
}

fun upisanaIstrazivanja() : List<Istrazivanje> {
    return listOf (
        Istrazivanje(1,"Istraživanje broj 1", 2),
        Istrazivanje(4,"Istraživanje broj 4", 4)
    )
}