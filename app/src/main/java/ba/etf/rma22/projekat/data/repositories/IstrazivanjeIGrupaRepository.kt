package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.AppDatabase
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.GrupaStudent
import ba.etf.rma22.projekat.data.models.Istrazivanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class IstrazivanjeIGrupaRepository {
    companion object {

        suspend fun getIstrazivanja(offset:Int=-1):List<Istrazivanje> {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                if(offset > -1) {
                    var response = ApiAdapter.retrofit.getAllIstrazivanja(offset)
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null)
                        db!!.istrazivanjeDao().insertAll(response)

                    return@withContext response
                } else {
                    var istrazivanja = mutableListOf<Istrazivanje>()
                    var i : Int = 1
                    while(true) {
                        var res = ApiAdapter.retrofit.getAllIstrazivanja(i)
                        istrazivanja.addAll(res)
                        if(res.size < 5)
                            break
                        i++
                    }
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null)
                        db!!.istrazivanjeDao().insertAll(istrazivanja)

                    return@withContext istrazivanja

                }
            } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var istrazivanja = db!!.istrazivanjeDao().getAll()
                        return@withContext istrazivanja
                    }
                return@withContext listOf()
            }
            }
        }

        suspend fun getIstrazivanjaByGodina(godina: Int):List<Istrazivanje> {
            return withContext(Dispatchers.IO) {
                if (MainActivity.connected) {

                    var istrazivanja = mutableListOf<Istrazivanje>()
                    var i: Int = 1
                    while (true) {
                        var res = ApiAdapter.retrofit.getAllIstrazivanja(i)
                        var tmp = res.filter { it.godina == godina }
                        istrazivanja.addAll(tmp)
                        if (res.size < 5)
                            break
                        i++
                    }

                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null)
                        db!!.istrazivanjeDao().insertAll(istrazivanja)

                    return@withContext istrazivanja

                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null) {
                        var istrazivanja = db!!.istrazivanjeDao().getIstrazivanjaGodine(godina)
                        return@withContext istrazivanja
                    }
                    return@withContext listOf()
                }
            }
            }


        private suspend fun getIstrazivanje(id: Int): Istrazivanje? {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                    try {
                        var response = ApiAdapter.retrofit.getIstrazivanjeById(id)
                        return@withContext response
                    } catch (ex: Exception) {
                        return@withContext null
                    }
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null) {
                        var istrazivanje = db!!.istrazivanjeDao().getIstrazivanje(id)
                        return@withContext istrazivanje
                    }
                    return@withContext null
                }
            }
        }

        suspend fun getGrupe():List<Grupa> {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                var response = ApiAdapter.retrofit.getAllGrupe()

                for(g in response) {
                    var istrazivanje = getIstrazivanje(g.istrazivanjeId!!)
                    if(istrazivanje!=null)
                        g.nazivIstrazivanja = istrazivanje.naziv
                }

                var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                if(db!=null)
                    db!!.grupaDao().insertAll(response)

                return@withContext response
            } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var grupe = db!!.grupaDao().getAll()
                        return@withContext grupe
                    }
                    return@withContext listOf()
            }
            }
        }


        suspend fun getGrupeZaIstrazivanje(idIstrazivanja:Int):List<Grupa> {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                var response = ApiAdapter.retrofit.getAllGrupe()

                for(g in response) {
                    var istrazivanje = getIstrazivanje(g.istrazivanjeId!!)
                    if(istrazivanje!=null)
                        g.nazivIstrazivanja = istrazivanje.naziv
                }

                var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                if(db!=null)
                    db!!.grupaDao().insertAll(response)

                return@withContext response.filter { it.istrazivanjeId == idIstrazivanja }
            } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var grupe = db!!.grupaDao().getGrupeIstrazivanja(idIstrazivanja)
                        return@withContext grupe
                    }
                    return@withContext listOf()
            }
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    var response = ApiAdapter.retrofit.postStudentToGrupa(idGrupa, AccountRepository.getHash())
                    if(response.poruka.contains("je dodan u grupu")) {
                        var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                        if(db!=null)
                            db!!.grupaStudentDao().insert(GrupaStudent(idGrupa, AccountRepository.acHash))
                        return@withContext true
                    }
                    return@withContext false
                } catch(ex: Exception) {
                    return@withContext false
                }
            }
        }

        suspend fun getUpisaneGrupe():List<Grupa> {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                var response = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())

                for(g in response) {
                    var istrazivanje = getIstrazivanje(g.istrazivanjeId!!)
                    if(istrazivanje!=null)
                        g.nazivIstrazivanja = istrazivanje.naziv

                    if(db!=null) {
                        db!!.grupaDao().insertAll(response)
                        db!!.grupaStudentDao().insert(GrupaStudent(g.id, AccountRepository.acHash))
                    }
                }

                return@withContext response
            } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    var grupe = mutableListOf<Grupa>()
                    if(db!=null) {
                        var gS = db!!.grupaStudentDao().getAll()
                        for(g in gS) {
                            var temp = db!!.grupaDao().getGrupa(g.grupaId)
                            grupe.add(temp)
                        }
                    }
                    return@withContext grupe
            }
            }
        }

        }
    }
