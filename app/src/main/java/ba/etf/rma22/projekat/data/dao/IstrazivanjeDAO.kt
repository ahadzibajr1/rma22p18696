package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Istrazivanje

@Dao
interface IstrazivanjeDAO {

    @Query("SELECT * FROM istrazivanje")
    suspend fun getAll() : List<Istrazivanje>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(istrazivanja: Istrazivanje)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(istrazivanja: List<Istrazivanje>)

    @Query("SELECT * FROM istrazivanje WHERE id=:id")
    suspend fun getIstrazivanje(id: Int) : Istrazivanje

    @Query("SELECT * FROM istrazivanje WHERE godina=:god")
    suspend fun getIstrazivanjaGodine(god: Int) : List<Istrazivanje>
}