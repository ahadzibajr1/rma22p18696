package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.GrupaAnketa

@Dao
interface GrupaAnketaDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(ga: GrupaAnketa)

    @Query("SELECT * FROM GrupaAnketa WHERE AnketaId=:anketaId")
    suspend fun getGrupeAnkete(anketaId: Int) : List<GrupaAnketa>

    @Query("SELECT * FROM GrupaAnketa WHERE GrupaId=:grupaId")
    suspend fun getAnketeGrupe(grupaId: Int) : List<GrupaAnketa>
}