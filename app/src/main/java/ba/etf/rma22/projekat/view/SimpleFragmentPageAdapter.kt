package ba.etf.rma22.projekat.view

import android.app.PendingIntent.getActivity
import android.util.Log
import android.util.Log.INFO
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import java.util.logging.Level.INFO


class SimpleFragmentPagerAdapter(
    fragmentManager: FragmentManager,
    var fragments: MutableList<Fragment>,
    lifecycle: Lifecycle
    ) : FragmentStateAdapter(fragmentManager, lifecycle) {

    fun add(index: Int, fragment: Fragment) {
        fragments.add(index, fragment)
        notifyItemChanged(index)
    }

    fun remove(index: Int) {
        fragments.removeAt(index)
        notifyItemChanged(index)
    }

    fun refreshFragment(index: Int, fragment: Fragment) {
        fragments[index] = fragment
        notifyItemChanged(index)
    }

    fun replaceFragments(fragmenti: MutableList<Fragment>) {
        val fragmentsSize = fragments.size
        val fragmentiSize = fragmenti.size
        if(fragmentsSize > 2)
            for ( i in 2 until fragmentsSize)
                fragments.removeAt(2)
        fragments[0] = fragmenti[0]
        fragments[1] = fragmenti[1]

        if(fragmentiSize > 2)
            for(i in 2 until fragmentiSize)
                fragments.add(i,fragmenti[i])

        notifyDataSetChanged()

    }

    override fun getItemId(position: Int): Long {
        return fragments[position].hashCode().toLong()
    }

    override fun containsItem(itemId: Long): Boolean {
        return fragments.find { it.hashCode().toLong() == itemId } != null
    }



    override fun createFragment(position: Int): Fragment {
        return fragments[position]

    }

    override fun getItemCount(): Int {
        return fragments.size
    }
}