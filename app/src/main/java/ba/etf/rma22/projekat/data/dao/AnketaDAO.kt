package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa

@Dao
interface AnketaDAO {
    @Query("SELECT * FROM anketa")
    suspend fun getAll() : List<Anketa>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(ankete: List<Anketa>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(anketa: Anketa)

    @Query("SELECT * FROM anketa WHERE id=:id")
    suspend fun getAnketa(id: Int) : Anketa
}