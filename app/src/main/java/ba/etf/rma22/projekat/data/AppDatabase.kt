package ba.etf.rma22.projekat.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ba.etf.rma22.projekat.data.dao.*
import ba.etf.rma22.projekat.data.models.*

@Database(entities = arrayOf(Account::class, Anketa::class, AnketaTaken::class,Grupa::class,Istrazivanje::class, Odgovor::class, PitanjeAnketa::class, Pitanje::class, GrupaAnketa::class, GrupaStudent::class ), version = 7)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun accountDao(): AccountDAO
    abstract fun anketaDao(): AnketaDAO
    abstract fun anketaTakenDao() : AnketaTakenDAO
    abstract fun grupaDao() : GrupaDAO
    abstract fun istrazivanjeDao() : IstrazivanjeDAO
    abstract fun odgovorDao() : OdgovorDAO
    abstract fun pitanjeAnketaDao(): PitanjeAnketaDAO
    abstract fun pitanjeDao() : PitanjeDAO
    abstract fun grupaAnketaDao() : GrupaAnketaDAO
    abstract fun grupaStudentDao(): GrupaStudentDAO
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = buildRoomDB(context)
                }
            }
            return INSTANCE!!
        }
        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "RMA22DB"
            ).build()
    }
}