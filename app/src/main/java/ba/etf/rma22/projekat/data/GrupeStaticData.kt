package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import java.util.*

fun sveGrupe() : List<Grupa> {

    return listOf(
        Grupa(0,"Grupa 1",null, "Istraživanje broj 0"),
        Grupa(1,"Grupa 1",null, "Istraživanje broj 1"),
        Grupa(2,"Grupa 2", null,"Istraživanje broj 1"),
        Grupa(3,"Grupa 3", null,"Istraživanje broj 2"),
        Grupa(4,"Grupa 1", null,"Istraživanje broj 3"),
        Grupa(5,"Grupa 0", null,"Istraživanje broj 4")
    )

}

fun upisaneGrupe() : List<Grupa> {
    return listOf(
    Grupa(1,"Grupa 1", null,"Istraživanje broj 1"),
    Grupa(5,"Grupa 0", null,"Istraživanje broj 4")
    )
}