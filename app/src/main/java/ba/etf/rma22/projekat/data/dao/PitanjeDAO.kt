package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Pitanje

@Dao
interface PitanjeDAO {

    @Query("SELECT * FROM pitanje")
    suspend fun getAll(): List<Pitanje>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pitanja: List<Pitanje>)

    @Query("SELECT * FROM pitanje WHERE AnketumId=:aid")
    suspend fun getPitanjaAnkete(aid : Int): List<Pitanje>


}