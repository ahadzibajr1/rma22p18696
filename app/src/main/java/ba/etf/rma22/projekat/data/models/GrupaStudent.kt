package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class GrupaStudent (
    @PrimaryKey var grupaId: Int,
    @ColumnInfo(name="acHash")var acHash: String
) {

}