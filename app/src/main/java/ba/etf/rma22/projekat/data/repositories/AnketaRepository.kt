package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.*
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.GrupaAnketa
import ba.etf.rma22.projekat.data.models.GrupaStudent
import ba.etf.rma22.projekat.data.repositories.GrupaRepository.Companion.getUpisaneGrupeOld
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.getUpisani
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*


class AnketaRepository {
    companion object {
        private var ankete : MutableList<Anketa>

        init {
            ankete = sveAnkete().toMutableList()
        }
        /*fun getAll(): List<Anketa> {
            //sve ankete
            return ankete.sortedBy{it.datumPocetak}
        }*/

        fun getMyAnkete(): List<Anketa> {
            //ankete za istrazivanja i grupe kojima korisnik pripada
            return ankete.filter {
                it.nazivIstrazivanja in getUpisani().map { it.naziv } &&
                        it.nazivGrupe in getUpisaneGrupeOld().map { it.naziv }
            }.sortedBy{it.datumPocetak}
        }

        fun getDone(): List<Anketa> {
            //sve urađene ankete
            return getMyAnkete().filter { it.datumRada != null }
        }

        fun getFuture(): List<Anketa> {
            //sve buduće ankete
            var cal = Calendar.getInstance()
            return getMyAnkete().filter { it.datumPocetak.after(cal.time) }
        }

        fun getNotTaken(): List<Anketa> {
            // sve neurađene ankete koje su prošle
            var cal = Calendar.getInstance()
            return getMyAnkete().filter { it.datumRada == null && (it.datumKraj!=null && it.datumKraj.before(cal.time)) }
        }

        suspend fun checkMyAnketa(anketa: Anketa) : Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    var upisaneAnkete = getUpisaneAnkete()
                    if(upisaneAnkete.find { it.naziv.equals(anketa.naziv) && it.nazivGrupe.equals(anketa.nazivGrupe)
                            && it.nazivIstrazivanja.equals(anketa.nazivIstrazivanja) } != null)
                                return@withContext true
                    return@withContext false
                } catch(ex: Exception) {
                    return@withContext false
                }

            }
        }

        fun odrediProgres ( progres : Float) : Int {
            if (progres > 0 && progres <= 0.2 )
                return 20
            else if (progres > 0.2 && progres <= 0.4)
                return 40
            else if (progres > 0.4 && progres <= 0.6)
                return 60
            else if (progres > 0.6 && progres <=0.8)
                return 80
            else if (progres > 0.8)
                return 100
            else
                return 0
        }

         fun odrediDatumIStatus(anketa: Anketa) : Pair<String,String> {
            val format = SimpleDateFormat("dd.MM.yyyy")
            if(anketa.datumPocetak.after(Calendar.getInstance().time)) //nije još aktivna, žuta
            {
                return Pair("Vrijeme aktiviranja: " + format.format(anketa.datumPocetak), "zuta")
            } else if (anketa.datumRada != null) //urađena, plava
            {
                return Pair("Anketa urađena: " + format.format(anketa.datumRada), "plava")
            } else if (anketa.datumKraj!=null && anketa.datumKraj.before(Calendar.getInstance().time)) //nije više aktivna, neurađena, crvena
            {
                return Pair("Anketa zatvorena: " + format.format(anketa.datumKraj), "crvena")
            }
             if(anketa.datumKraj != null)
                return Pair("Vrijeme zatvaranja: " + format.format(anketa.datumKraj), "zelena") //aktivna, neurađena
             else
                 return Pair("Vrijeme zatvaranja: ", "zelena")

        }

        fun updateProgres(anketa: Anketa, progres: Float) {
            ankete.find{it -> it == anketa}?.progres  = progres
        }

        fun updateDatumRada(anketa: Anketa) {
            ankete.find{it -> it == anketa}?.datumRada = Calendar.getInstance().time
        }

        fun vratiNaDefault() {
            ankete = sveAnkete().toMutableList()
        }

        suspend fun getAll(offset:Int=-1):List<Anketa> {
           return withContext(Dispatchers.IO) {
               if(MainActivity.connected) {
                   if (offset > -1) {
                       var response = ApiAdapter.retrofit.getAllAnkete(offset)
                       var takenAnkete =
                           ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                               .sortedByDescending { it.id }

                       for (a in response) {
                           var ta = takenAnkete.find { it.AnketumId == a.id }
                           if (ta != null) {
                               a.progres = ta.progres / 100f
                               a.datumRada = ta.datumRada
                           }

                       }

                       var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                       if (db != null)
                           db!!.anketaDao().insertAll(response)

                       return@withContext response
                   } else {
                       var ankete = mutableListOf<Anketa>()
                       var takenAnkete =
                           ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                               .sortedByDescending { it.id }
                       var i: Int = 1
                       while (true) {
                           var res = ApiAdapter.retrofit.getAllAnkete(i)
                           ankete.addAll(res)
                           if (res.size < 5)
                               break
                           i++
                       }

                       for (a in ankete) {
                           var ta = takenAnkete.find { it.AnketumId == a.id }
                           if (ta != null) {
                               a.progres = ta.progres / 100f
                               a.datumRada = ta.datumRada
                           }

                       }


                       var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                       if (db != null)
                           db!!.anketaDao().insertAll(ankete)
                       return@withContext ankete

                   }
               } else {
                   //dobavljamo iz baze
                   var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                   var ankete = listOf<Anketa>()
                   if(db!=null)
                       ankete = db!!.anketaDao().getAll()

                   return@withContext ankete
               }
            }
        }

        suspend fun getAllAnkete() : List<Anketa> {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                    try {
                        var ankete = getAll()
                        var sveAnkete = mutableListOf<Anketa>()
                        var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                        for (a in ankete) {
                            var grupe = ApiAdapter.retrofit.getGrupeOfAnketa(a.id)
                            for (g in grupe) {
                                var istrazivanje = ApiAdapter.retrofit.getIstrazivanjeOfGrupa(g.id)
                                g.nazivIstrazivanja = istrazivanje.naziv
                                if (db != null)
                                    db!!.istrazivanjeDao().insert(istrazivanje)
                                var temp: Anketa = Anketa(
                                    a.id,
                                    a.naziv,
                                    istrazivanje.naziv,
                                    a.datumPocetak,
                                    a.datumKraj,
                                    a.datumRada,
                                    a.trajanje,
                                    g.naziv,
                                    a.progres
                                )
                                sveAnkete.add(temp)
                                if (db != null)
                                    db!!.grupaAnketaDao().insert(
                                        GrupaAnketa(
                                            g.id,
                                            a.id,
                                            istrazivanje.id,
                                            g.naziv,
                                            istrazivanje.naziv
                                        )
                                    )
                            }
                            if (db != null)
                                db!!.grupaDao().insertAll(grupe)
                        }




                        return@withContext sveAnkete
                    } catch (ex: Exception) {
                        return@withContext listOf()
                    }
                } else {
                    var ankete = getAll()
                    var sveAnkete = mutableListOf<Anketa>()
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!= null) {
                        for(a in ankete) {
                            var gA = db!!.grupaAnketaDao().getGrupeAnkete(a.id)
                            for(g in gA) {
                                var temp: Anketa = Anketa(
                                    a.id,
                                    a.naziv,
                                    g.istrazivanje,
                                    a.datumPocetak,
                                    a.datumKraj,
                                    a.datumRada,
                                    a.trajanje,
                                    g.grupa,
                                    a.progres
                                )
                                sveAnkete.add(temp)
                            }
                        }
                    }
                    return@withContext sveAnkete
                }

            }
        }

        suspend fun getById(id:Int):Anketa? {
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                    try {
                        var response = ApiAdapter.retrofit.getAnketa(id)
                        var takenAnketa =
                            ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                                .find { it.AnketumId == id }
                        var grupa = ApiAdapter.retrofit.getGrupeOfAnketa(id).first()
                        var istrazivanje = ApiAdapter.retrofit.getIstrazivanjeOfGrupa(grupa.id)

                        if (takenAnketa != null) {
                            response.progres = takenAnketa.progres / 100f
                            response.datumRada = takenAnketa.datumRada
                        }

                        response.nazivGrupe = grupa.naziv
                        response.nazivIstrazivanja = istrazivanje.naziv

                        var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                        if (db != null)
                            db!!.anketaDao().insertAll(listOf(response))

                        return@withContext response
                    } catch (ex: Exception) {
                        return@withContext null
                    }
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var anketa = db!!.anketaDao().getAnketa(id)
                        return@withContext anketa
                    }

                    return@withContext null
                }
            }
        }

        suspend fun getUpisane():List<Anketa> {
            return withContext(Dispatchers.IO) {
                if (MainActivity.connected) {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    var upisaneAnkete = mutableListOf<Anketa>()
                    var grupe = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
                    var takenAnkete =
                        ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                            .sortedByDescending { it.id }
                    if (db != null)
                        db!!.anketaTakenDao().insertAll(takenAnkete)


                    for (g in grupe) {
                        var anketeGrupe = ApiAdapter.retrofit.getAnketeOfGrupa(g.id)

                        upisaneAnkete.addAll(anketeGrupe)
                        if (db != null)
                            db!!.grupaStudentDao()
                                .insert(GrupaStudent(g.id, AccountRepository.acHash))
                    }

                    var anketeUpisane = mutableListOf<Anketa>()
                    for (a in upisaneAnkete) {
                        var ta = takenAnkete.find { it.AnketumId == a.id }
                        if (ta != null) {
                            a.progres = ta.progres / 100f
                            a.datumRada = ta.datumRada
                        }
                        anketeUpisane.add(a)
                    }
                    if (db != null)
                        db!!.anketaDao().insertAll(anketeUpisane)
                    return@withContext anketeUpisane
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var upisaneAnkete = mutableListOf<Anketa>()
                        var gS = db!!.grupaStudentDao().getAll()
                        for(g in gS) {
                            var gA = db!!.grupaAnketaDao().getAnketeGrupe(g.grupaId)
                            for(a in gA) {
                                var anketa = db!!.anketaDao().getAnketa(a.anketaId)
                                upisaneAnkete.add(anketa)
                            }
                        }
                        var takenAnkete = db!!.anketaTakenDao().getAll()
                        var anketeUpisane = mutableListOf<Anketa>()
                        for (a in upisaneAnkete) {
                            var ta = takenAnkete.find { it.AnketumId == a.id }
                            if (ta != null) {
                                a.progres = ta.progres / 100f
                                a.datumRada = ta.datumRada
                            }
                            anketeUpisane.add(a)
                        }

                        return@withContext anketeUpisane

                    }

                    return@withContext listOf()
                }
            }
        }

        suspend fun getUpisaneAnkete():List<Anketa> {
            return withContext(Dispatchers.IO) {
                if (MainActivity.connected) {
                    getUpisane()
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    var upisaneAnkete = mutableListOf<Anketa>()
                    var grupe = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
                    var takenAnkete =
                        ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                            .sortedByDescending { it.id }
                    if (db != null)
                        db!!.anketaTakenDao().insertAll(takenAnkete)

                    for (g in grupe) {
                        var anketeGrupe = ApiAdapter.retrofit.getAnketeOfGrupa(g.id)
                        var istrazivanje = ApiAdapter.retrofit.getIstrazivanjeOfGrupa(g.id)
                        if (db != null)
                            db!!.istrazivanjeDao().insert(istrazivanje)
                        g.nazivIstrazivanja = istrazivanje.naziv
                        if (db != null)
                            db!!.grupaStudentDao()
                                .insert(GrupaStudent(g.id, AccountRepository.acHash))
                        for (a in anketeGrupe) {
                            var ta = takenAnkete.find { it.AnketumId == a.id }
                            if (ta != null) {
                                a.progres = ta.progres / 100f
                                a.datumRada = ta.datumRada
                            }
                            if (db != null)
                                db!!.anketaDao().insert(a)
                            a.nazivIstrazivanja = istrazivanje.naziv
                            a.nazivGrupe = g.naziv
                            upisaneAnkete.add(a)
                            if (db != null)
                                db!!.grupaAnketaDao().insert(
                                    GrupaAnketa(
                                        g.id,
                                        a.id,
                                        istrazivanje.id,
                                        g.naziv,
                                        istrazivanje.naziv
                                    )
                                )
                        }

                    }
                    if (db != null)
                        db!!.grupaDao().insertAll(grupe)

                    return@withContext upisaneAnkete
                } else {

                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var upisaneAnkete = mutableListOf<Anketa>()
                        var gS = db!!.grupaStudentDao().getAll()
                        for(g in gS) {
                            var gA = db!!.grupaAnketaDao().getAnketeGrupe(g.grupaId)
                            for(a in gA) {
                                var anketa = db!!.anketaDao().getAnketa(a.anketaId)
                                anketa.nazivIstrazivanja = a.istrazivanje
                                anketa.nazivGrupe = a.grupa
                                upisaneAnkete.add(anketa)
                            }
                        }
                        var takenAnkete = db!!.anketaTakenDao().getAll()
                        var anketeUpisane = mutableListOf<Anketa>()
                        for (a in upisaneAnkete) {
                            var ta = takenAnkete.find { it.AnketumId == a.id }
                            if (ta != null) {
                                a.progres = ta.progres / 100f
                                a.datumRada = ta.datumRada
                            }
                            anketeUpisane.add(a)
                        }
                        return@withContext anketeUpisane
                    }

                    return@withContext listOf()
                }
            }
        }

        suspend fun getUrađene():List<Anketa> {
            return withContext(Dispatchers.IO) {
                if (MainActivity.connected) {
                    var urađeneAnkete = mutableListOf<Anketa>()
                    var takenAnkete =
                        ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())
                            .sortedByDescending { it.id }
                    var grupe = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.acHash)

                    for (ta in takenAnkete) {
                        var odgovori =
                            ApiAdapter.retrofit.getOdgovori(AccountRepository.getHash(), ta.id)
                        var pitanja = ApiAdapter.retrofit.getPitanja(ta.AnketumId)
                        var anketa = ApiAdapter.retrofit.getAnketa(ta.AnketumId)

                        if (odgovori.size == pitanja.size || (anketa.datumKraj != null && anketa.datumKraj!!.before(
                                Calendar.getInstance().time
                            ))
                        )
                            if (!urađeneAnkete.map { it.id }.contains(anketa.id)) {
                                anketa.progres = ta.progres / 100f
                                anketa.datumRada = ta.datumRada

                                var grupeOfAnketa = ApiAdapter.retrofit.getGrupeOfAnketa(anketa.id)
                                    .filter { grupe.contains(it) }

                                for (g in grupeOfAnketa) {
                                    var istrazivanje =
                                        ApiAdapter.retrofit.getIstrazivanjeOfGrupa(g.id)
                                    var temp = Anketa(
                                        anketa.id,
                                        anketa.naziv,
                                        istrazivanje.naziv,
                                        anketa.datumPocetak,
                                        anketa.datumKraj,
                                        anketa.datumRada,
                                        anketa.trajanje,
                                        g.naziv,
                                        anketa.progres
                                    )
                                    urađeneAnkete.add(temp)
                                }

                            }
                    }



                    return@withContext urađeneAnkete
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var urađeneAnkete = mutableListOf<Anketa>()
                        var takenAnkete = db!!.anketaTakenDao().getAll().sortedByDescending { it.id }
                        var gS = db!!.grupaStudentDao().getAll()
                        var grupe = mutableListOf<Grupa>()
                        for(g in gS) {
                            var grupa = db!!.grupaDao().getGrupa(g.grupaId)
                            grupe.add(grupa)
                        }

                        for (ta in takenAnkete) {
                            var odgovori = db!!.odgovorDao().getOdgovoriAnkete(ta.id)
                            var pitanja = db!!.pitanjeDao().getPitanjaAnkete(ta.AnketumId)
                            var anketa = db!!.anketaDao().getAnketa(ta.AnketumId)

                            if (odgovori.size == pitanja.size || (anketa.datumKraj != null && anketa.datumKraj!!.before(
                                    Calendar.getInstance().time
                                ))
                            )
                                if (!urađeneAnkete.map { it.id }.contains(anketa.id)) {
                                    anketa.progres = ta.progres / 100f
                                    anketa.datumRada = ta.datumRada

                                    var gA = db!!.grupaAnketaDao().getGrupeAnkete(anketa.id)

                                    for (g in gA) {
                                        if(!(g.grupaId in grupe.map { it.id })) continue
                                        var temp = Anketa(
                                            anketa.id,
                                            anketa.naziv,
                                            g.istrazivanje,
                                            anketa.datumPocetak,
                                            anketa.datumKraj,
                                            anketa.datumRada,
                                            anketa.trajanje,
                                            g.grupa,
                                            anketa.progres
                                        )
                                        urađeneAnkete.add(temp)
                                    }

                                }
                        }
                        return@withContext urađeneAnkete
                    }

                    return@withContext listOf()
                }
            }
        }

        suspend fun getProšle():List<Anketa> {
            //još treba provjeriti
            return withContext(Dispatchers.IO) {
                /*var urađeneAnkete = mutableListOf<Anketa>()
                var takenAnkete = ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())

                for(ta in takenAnkete) {
                    var odgovori = ApiAdapter.retrofit.getOdgovori(AccountRepository.getHash(),ta.id)
                    var pitanja = ApiAdapter.retrofit.getPitanja( ta.anketumId)
                    var anketa = ApiAdapter.retrofit.getAnketa(ta.anketumId)

                    if(odgovori.size ==0 && (anketa.datumKraj!= null && anketa.datumKraj!!.before(Calendar.getInstance().time)))
                        if(!urađeneAnkete.contains(anketa))
                            urađeneAnkete.add(anketa)
                }*/

                if(MainActivity.connected) {
                    var prosleAnkete = mutableListOf<Anketa>()
                    var grupe = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())

                    for (g in grupe) {
                        var istrazivanje = ApiAdapter.retrofit.getIstrazivanjeOfGrupa(g.id)
                        var anketeGrupe = ApiAdapter.retrofit.getAnketeOfGrupa(g.id)
                            .filter { it.datumKraj != null && it.datumKraj!!.before(Calendar.getInstance().time) && it.datumRada == null }
                        for (a in anketeGrupe) {
                            a.nazivIstrazivanja = istrazivanje.naziv
                            a.nazivGrupe = g.naziv
                            prosleAnkete.add(a)
                        }

                    }
                    return@withContext prosleAnkete
                } else {
                    var prosleAnkete = mutableListOf<Anketa>()
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var gS = db!!.grupaStudentDao().getAll()

                        for(g in gS) {
                            var grupa = db!!.grupaDao().getGrupa(g.grupaId)
                            var gA = db!!.grupaAnketaDao().getAnketeGrupe(grupa.id)
                            for(a in gA) {
                                var anketa = db!!.anketaDao().getAnketa(a.anketaId)
                                if(anketa.datumKraj != null && anketa.datumKraj!!.before(Calendar.getInstance().time) && anketa.datumRada == null) {
                                    anketa.nazivGrupe = a.grupa
                                    anketa.nazivIstrazivanja = a.istrazivanje
                                    prosleAnkete.add(anketa)
                                }

                            }
                        }

                    }
                    return@withContext prosleAnkete
                }


            }
        }

        suspend fun getBuduće(): List<Anketa> {
            return withContext(Dispatchers.IO) {
            if (MainActivity.connected) {
                var buduceAnkete = mutableListOf<Anketa>()
                var grupe = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())

                for (g in grupe) {
                    var istrazivanje = ApiAdapter.retrofit.getIstrazivanjeOfGrupa(g.id)
                    var anketeGrupe = ApiAdapter.retrofit.getAnketeOfGrupa(g.id)
                        .filter { it.datumPocetak.after(Calendar.getInstance().time) }
                    for (a in anketeGrupe) {
                        a.nazivIstrazivanja = istrazivanje.naziv
                        a.nazivGrupe = g.naziv
                        buduceAnkete.add(a)
                    }

                }

                return@withContext buduceAnkete
            } else {
                var buduceAnkete = mutableListOf<Anketa>()
                var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                if(db!=null) {
                    var gS = db!!.grupaStudentDao().getAll()

                    for(g in gS) {
                        var grupa = db!!.grupaDao().getGrupa(g.grupaId)
                        var gA = db!!.grupaAnketaDao().getAnketeGrupe(grupa.id)
                        for(a in gA) {
                            var anketa = db!!.anketaDao().getAnketa(a.anketaId)
                            if(anketa.datumPocetak.after(Calendar.getInstance().time)) {
                                anketa.nazivGrupe = a.grupa
                                anketa.nazivIstrazivanja = a.istrazivanje
                                buduceAnkete.add(anketa)
                            }

                        }
                    }

                }
                return@withContext buduceAnkete
            }
            }
        }
    }
}