package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Account
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel {
    companion object {
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        fun postaviHash(hash: String, context: Context, onSuccess: () -> Unit,
                        onError: () -> Unit){
            scope.launch{
                AccountRepository.context = context
                val result = AccountRepository.postaviHash(hash)
                if (result == true) onSuccess?.invoke()
                else onError?.invoke()
            }
        }


    }

}