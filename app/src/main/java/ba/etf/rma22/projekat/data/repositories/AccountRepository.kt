package ba.etf.rma22.projekat.data.repositories


import android.content.Context
import android.util.Log
import androidx.room.Room
import ba.etf.rma22.projekat.data.AppDatabase
import ba.etf.rma22.projekat.data.models.Account
import ba.etf.rma22.projekat.viewmodel.AnketaTakenViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepository {
    companion object {
        var acHash: String = "d501e8ae-e0a1-42e5-882a-79b682337974"
        var context: Context? = null

        suspend fun postaviHash(hash: String): Boolean {

            return withContext(Dispatchers.IO) {
                try {
                    var db = context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var prevAccounts = db!!.accountDao().getAccounts()
                        if (prevAccounts != null && prevAccounts.size > 0)
                            db!!.accountDao().deleteAccounts(prevAccounts[0])
                        db!!.clearAllTables()
                        db!!.accountDao().insertAccount(Account(hash))
                    }
                    /*var poceteAnkete = TakeAnketaRepository.getPoceteAnkete()
                    if (poceteAnkete != null) {
                        db!!.anketaTakenDao().insertAll(poceteAnkete)

                        for (at in poceteAnkete) {
                            var odgovori = OdgovorRepository.getOdgovoriAnketaTaken(at.id)
                            var pitanja = PitanjeAnketaRepository.getPitanja(at.AnketumId)
                            db!!.odgovorDao().insertAll(odgovori)
                        }
                    }*/


                    acHash = hash
                    return@withContext true
                } catch (ex: Exception) {
                    return@withContext false
                }
            }
        }



        fun getHash(): String {
            return acHash
            //return "d501e8ae-e0a1-42e5-882a-79b682337974" //moj hash
        }
    }
}