package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.viewmodel.AnketaTakenViewModel
import java.util.*


class FragmentPitanje(
    val pitanje: Pitanje,
    val anketaTaken: AnketaTaken,
    val anketa: Anketa
    ): Fragment() {
    private lateinit var listaOdgovora : ListView
    private lateinit var adapter: MyArrayAdapter
    private var odabrani: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.pitanje_fragment, container, false)

        val tekst = view.findViewById<TextView>(R.id.tekstPitanja)
        tekst.text = pitanje.tekstPitanja

        listaOdgovora = view.findViewById<ListView>(R.id.odgovoriLista)

        AnketaTakenViewModel.getOdgovor(anketaTaken.id, pitanje.id, ::onSuccessOdgovor, ::onError)


        val dugmeZaustavi = view.findViewById<Button>(R.id.dugmeZaustavi)
        dugmeZaustavi.setOnClickListener { l ->
            MainActivity.zaustaviAnketu()
        }

        return view
    }



    suspend fun onSuccessOdgovor(odg: Odgovor?) {
            adapter = MyArrayAdapter(requireActivity(), R.layout.opcija_item, pitanje.opcije)
            listaOdgovora.adapter = adapter
            listaOdgovora.setChoiceMode(ListView.CHOICE_MODE_SINGLE)


            if(odg!=null) {
                FragmentAnkete.brojPrethodnoOdgovorenih +=1
                listaOdgovora.setItemChecked(odg.odgovoreno, true)
                listaOdgovora.isClickable = false
                listaOdgovora.isEnabled = false
            } else if(anketa.datumKraj!=null &&  anketa.datumKraj!!.before(Calendar.getInstance().time)){
                listaOdgovora.isClickable = false
                listaOdgovora.isEnabled = false
            } else if(MainActivity.connected == false) {
                listaOdgovora.isClickable = false
                listaOdgovora.isEnabled = false
            }
            else {
                listaOdgovora.onItemClickListener = AdapterView.OnItemClickListener{parent, view, position, id ->
                var temp = FragmentAnkete.odgovoriIPitanja.find { it.first == pitanje }
                FragmentAnkete.odgovoriIPitanja.remove(temp)
                FragmentAnkete.odgovoriIPitanja.add(Pair(pitanje,position))
                //AnketaTakenViewModel.dodajOdgovor(anketaTaken.id, pitanje.id, position, ::onSuccessOdgovoreno, ::onError)
                odabrani = position
                listaOdgovora.setItemChecked(odabrani!!, true)
            }



        }
    }

    fun onSuccessOdgovoreno() {
        listaOdgovora.setItemChecked(odabrani!!, true)
        listaOdgovora.isClickable = false
        listaOdgovora.isEnabled = false
        adapter.notifyDataSetChanged()
    }

    fun onError() {
        Toast.makeText(this.context, "Error", Toast.LENGTH_SHORT)
    }

    companion object {
        fun newInstance(pitanje: Pitanje, anketaTaken: AnketaTaken, anketa: Anketa): FragmentPitanje = FragmentPitanje(pitanje,anketaTaken, anketa)
    }
}