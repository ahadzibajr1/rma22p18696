package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.AppDatabase
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
import ba.etf.rma22.projekat.data.pitanjaPoAnketama
import ba.etf.rma22.projekat.data.svaPitanja
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeAnketaRepository() {

    companion object {

        private var odgovorenaPitanja : MutableList<PitanjeAnketa>
        private var pitanja : MutableList<Pitanje>
        private var pitanjaPoAnketama : MutableList<PitanjeAnketa>
        init {
            odgovorenaPitanja = mutableListOf()
            pitanja = svaPitanja()
            pitanjaPoAnketama = pitanjaPoAnketama()
        }

        fun getPitanja(nazivAnkete: String, nazivIstrazivanja: String): List<Pitanje> {
            return pitanja.filter {
                it.naziv in pitanjaPoAnketama.filter {
                    it.anketa.equals(nazivAnkete) && it.istrazivanje.equals(nazivIstrazivanja)
                }.map { it.naziv }
            }
        }

        fun odgovoriNaPitanje(naziv: String, nazivAnkete: String, nazivIstrazivanja: String, opcija: Int) {
           var pitanje = odgovorenaPitanja.find { it-> it.naziv.equals(naziv) && it.anketa.equals(nazivAnkete) && it.istrazivanje.equals(nazivIstrazivanja) }
            if(pitanje != null) {
                pitanje.odabranaOpcija = opcija
            } else {
                odgovorenaPitanja.add(PitanjeAnketa(Math.random().toInt(),0,0,naziv,nazivAnkete, nazivIstrazivanja, opcija))
            }
        }

        fun getOdgovor(naziv: String, nazivAnkete: String, nazivIstrazivanja: String): Int?  {
            return odgovorenaPitanja.find { it-> it.naziv.equals(naziv) && it.anketa.equals(nazivAnkete) && it.istrazivanje.equals(nazivIstrazivanja) }?.odabranaOpcija
        }

        fun getBrojOdgovora(nazivAnkete: String, nazivIstrazivanja: String) : Int {
            return odgovorenaPitanja.count{it -> it.anketa.equals(nazivAnkete) && it.istrazivanje.equals(nazivIstrazivanja)}
        }

        fun getBrojPitanja(nazivAnkete: String, nazivIstrazivanja: String): Int {
            return pitanjaPoAnketama.count{it -> it.anketa.equals(nazivAnkete) && it.istrazivanje.equals(nazivIstrazivanja)}
        }

        fun vratiNaDefault() {
            odgovorenaPitanja = mutableListOf()
            pitanja = svaPitanja()
            pitanjaPoAnketama = pitanjaPoAnketama()
        }

        suspend fun getPitanja(idAnkete: Int) : List<Pitanje> {
            //vraca sva pitanja ankete s proslijedjenim id
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                    var response = ApiAdapter.retrofit.getPitanja(idAnkete)

                    if (response == null) return@withContext listOf()
                    response.map { it.AnketumId = idAnkete }
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null)
                        db!!.pitanjeDao().insertAll(response)

                    return@withContext response
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null) {
                        var pitanja = db!!.pitanjeDao().getPitanjaAnkete(idAnkete)
                        return@withContext pitanja
                    }
                    return@withContext listOf()
                }
            }
        }


    }

}