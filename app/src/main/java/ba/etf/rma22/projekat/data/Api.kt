package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.*
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @GET("/anketa/{id}/pitanja")
    suspend fun getPitanja(@Path("id") id: Int) : List<Pitanje>

    @POST("/student/{id}/anketa/{kid}")
    suspend fun takeAnketa(@Path("id") acHash: String, @Path("kid") anketaId: Int): AnketaTaken

    @GET("/student/{id}/anketataken")
    suspend fun getTakenAnkete(@Path("id") acHash: String): List<AnketaTaken>

    @GET("/student/{id}/anketataken/{ktid}/odgovori")
    suspend fun getOdgovori(@Path("id") acHash:String, @Path("ktid") anketaId: Int): List<Odgovor>

    @POST("/student/{id}/anketataken/{ktid}/odgovor")
      suspend fun postOdgovor(@Path("id") acHash:String, @Path("ktid") anketaId: Int, @Body odgovor: OdgovorData): Odgovor

    @GET("/anketa")
    suspend fun getAllAnkete(@Query("offset") offset: Int?): List<Anketa>

    @GET("/anketa/{id}")
    suspend fun getAnketa(@Path("id") id: Int): Anketa

    @GET("/grupa/{id}/ankete")
    suspend fun getAnketeOfGrupa(@Path("id") grupaId: Int): List<Anketa>

    @GET("/istrazivanje")
    suspend fun getAllIstrazivanja(@Query("offset") offset: Int?): List<Istrazivanje>

    @GET("/istrazivanje/{id}")
    suspend fun getIstrazivanjeById(@Path("id") istrazivanjeId: Int): Istrazivanje

    @GET("/grupa")
    suspend fun getAllGrupe(): List<Grupa>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun postStudentToGrupa(@Path("gid") grupaId: Int, @Path("id") acHash: String): ResponsePoruka

    @GET("/student/{id}/grupa")
    suspend fun getUpisaneGrupe(@Path("id") acHash: String): List<Grupa>

    @GET("/anketa/{id}/grupa")
    suspend fun getGrupeOfAnketa(@Path("id") id: Int): List<Grupa>

    @GET("/grupa/{gid}/istrazivanje")
    suspend fun getIstrazivanjeOfGrupa(@Path("gid") grupaId: Int) : Istrazivanje

}