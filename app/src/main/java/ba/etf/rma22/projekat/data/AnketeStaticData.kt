package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Anketa
import java.util.*

fun sveAnkete() : List<Anketa> {
    val cal : Calendar = Calendar.getInstance()
    cal.set(2022,2,10)
    val datumPrije = cal.getTime()
    cal.set(2022,11,10)
    val datumPoslije = cal.getTime()
    cal.set(2022,2,20)
    val datumRada = cal.getTime()

    cal.set(2021,10,10)
    val prviDatum = cal.getTime()


    return listOf(
        Anketa(0,"Anketa 1", "Istraživanje broj 1", datumPrije,datumPoslije, datumRada,10,"Grupa 1",0.0F ),
        Anketa(1,"Anketa 1", "Istraživanje broj 2", datumPrije,datumPoslije, null,10,"Grupa 3",0.0F ),
        Anketa(2,"Anketa 1", "Istraživanje broj 3", datumPrije,datumPoslije, datumRada,10,"Grupa 1",0.0F ),
        Anketa(3,"Anketa 2", "Istraživanje broj 1", datumPoslije,datumPoslije, null,20,"Grupa 2",0.0F ),
        Anketa(4,"Anketa 1", "Istraživanje broj 0", datumPrije,datumPrije, null,10,"Grupa 1",0.0F ),
        Anketa(5,"Anketa 3", "Istraživanje broj 4", datumPrije,datumPrije, null,10,"Grupa 0",0.0F ),
        Anketa(6,"Anketa 4", "Istraživanje broj 4", datumPoslije,datumPoslije, null,10,"Grupa 0",0.0F ),
        Anketa(7,"Anketa 3", "Istraživanje broj 1", datumPoslije,datumPoslije, null,10,"Grupa 1",0.0F ),
        Anketa(8,"Anketa 4", "Istraživanje broj 1", prviDatum,datumPoslije, null,10,"Grupa 1",0.0F )
    )
}