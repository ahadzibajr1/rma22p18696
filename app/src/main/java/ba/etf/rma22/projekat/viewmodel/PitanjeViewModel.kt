package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.reflect.KSuspendFunction2

class PitanjeViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun dohvatiPitanja(id: Int, onSuccess: KSuspendFunction2<List<Pitanje>, Int, Unit>, onError: () ->Unit) {
        scope.launch {
            val res = PitanjeAnketaRepository.getPitanja(id)

            when(res) {
                is List<Pitanje> -> onSuccess.invoke(res,id)
                else -> onError.invoke()

            }
        }
    }
}