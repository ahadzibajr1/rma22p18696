package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.AppDatabase
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.OdgovorData
import ba.etf.rma22.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Integer.parseInt

class OdgovorRepository {

    companion object {
        suspend fun getOdgovoriAnketa(idAnkete:Int):List<Odgovor> {
            //vraca listu odgovora za anketu ili praznu listu ako student nije upisan ili nije odgovarao
            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                try {
                    var takenAnkete : List<AnketaTaken> = ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash()).filter{it.AnketumId == idAnkete}

                    if(takenAnkete.isEmpty()) return@withContext listOf()

                    var odgovori = mutableListOf<Odgovor>()

                    for(ta in takenAnkete) {
                        var odg = ApiAdapter.retrofit.getOdgovori(AccountRepository.getHash(), ta.id)
                        odgovori.addAll(odg)
                    }

                    return@withContext odgovori
                } catch (ex: Exception) {
                    return@withContext listOf()
                }
            } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var takenAnkete = db!!.anketaTakenDao().getAll()
                        var odgovori = mutableListOf<Odgovor>()
                        for(ta in takenAnkete) {
                            var odg = db!!.odgovorDao().getOdgovoriAnkete(ta.id)
                            odgovori.addAll(odg)
                        }
                        return@withContext odgovori
                    }
                    return@withContext listOf()
            }
            }
        }

        suspend fun getOdgovoriAnketaTaken(ktid:Int):List<Odgovor> {

            return withContext(Dispatchers.IO) {
                if(MainActivity.connected) {
                try {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    var odgovori = ApiAdapter.retrofit.getOdgovori(AccountRepository.getHash(), ktid)
                    if(db!=null)
                        db!!.odgovorDao().insertAll(odgovori)

                    return@withContext odgovori
                } catch (ex: Exception) {
                    return@withContext listOf()
                }
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        var odgovori = db!!.odgovorDao().getOdgovoriAnkete(ktid)
                        return@withContext odgovori
                    }
                    return@withContext listOf()
                }
            }
        }

        suspend fun postaviOdgovorAnketa(idAnketaTaken:Int,idPitanje:Int,odgovor:Int):Int {
            //postavlja odgovor na pitanje
            return withContext(Dispatchers.IO) {
                try {
                    var odgovori = getOdgovoriAnketaTaken(idAnketaTaken)
                    var ta = TakeAnketaRepository.getPoceteAnkete()?.find { it.id == idAnketaTaken }
                    var pitanja = PitanjeAnketaRepository.getPitanja(ta!!.AnketumId)
                    var temp: Float = ((odgovori.size + 1f) / pitanja.size)
                    var noviProgres = AnketaRepository.odrediProgres(temp)

                    var odgovorData: OdgovorData = OdgovorData(odgovor, idPitanje, noviProgres)
                    var response = ApiAdapter.retrofit.postOdgovor(
                        AccountRepository.getHash(),
                        idAnketaTaken,
                        odgovorData
                    )

                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if(db!=null) {
                        db!!.odgovorDao().insert(response)
                        db!!.anketaTakenDao().update(response.anketaTakenId, noviProgres)
                    }
                    return@withContext noviProgres
                } catch (ex: Exception) {
                    return@withContext -1
                }
            }
        }
    }
}