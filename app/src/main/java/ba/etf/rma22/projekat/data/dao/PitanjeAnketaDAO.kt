package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.PitanjeAnketa

@Dao
interface PitanjeAnketaDAO {
    @Query("SELECT * FROM pitanjeanketa")
    suspend fun getAll(): List<PitanjeAnketa>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pitanjaAnkete: List<PitanjeAnketa>)
}