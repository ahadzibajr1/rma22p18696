package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
data class PitanjeAnketa(
    @PrimaryKey var id: Int,
    @ColumnInfo(name="AnketumId") @SerializedName("AnketumId") val anketumId: Int,
    @ColumnInfo(name="PitanjeId") @SerializedName("PitanjeId") val pitanjeId: Int,
    @ColumnInfo(name="naziv") val naziv: String?,
    @ColumnInfo(name="anketa") val anketa: String?,
    @ColumnInfo(name="istrazivanje") val istrazivanje: String?,
    @ColumnInfo(name="odabranaOpcija") var odabranaOpcija: Int? = null
    )
    {
    }