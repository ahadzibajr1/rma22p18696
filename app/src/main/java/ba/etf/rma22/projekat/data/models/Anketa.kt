package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Anketa(
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo(name="naziv") @SerializedName("naziv") val naziv : String,
    @ColumnInfo(name="nazivIstrazivanja") var nazivIstrazivanja : String?,
    @ColumnInfo(name="datumPocetak") @SerializedName("datumPocetak") val datumPocetak: Date,
    @ColumnInfo(name="datumKraj") @SerializedName("datumKraj") val datumKraj: Date?,
    @ColumnInfo(name="datumRada") var datumRada: Date?,
    @ColumnInfo(name="trajanje") @SerializedName("trajanje") val trajanje: Int,
    @ColumnInfo(name="nazivGrupe") var nazivGrupe: String?,
    @ColumnInfo(name="progres") var progres: Float = 0f
) {
}