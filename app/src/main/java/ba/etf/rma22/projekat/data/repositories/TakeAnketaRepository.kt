package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.AppDatabase
import ba.etf.rma22.projekat.data.models.AnketaTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class TakeAnketaRepository {
    companion object {
       suspend fun zapocniAnketu (idAnkete: Int): AnketaTaken? {
            //zapocinje anketu, ili vraca null u slucaju greske
           return withContext(Dispatchers.IO) {
               try {
                   var response =
                       ApiAdapter.retrofit.takeAnketa(AccountRepository.getHash(), idAnkete)

                   var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                   if(db!=null)
                    db!!.anketaTakenDao().insert(response)
                   return@withContext response
               } catch (ex: Exception) {
                   return@withContext null
               }
           }
        }

        suspend fun getPoceteAnkete():List<AnketaTaken>? {
            //vraca zapocete ankete ili null ukoliko ih nema
            return withContext(Dispatchers.IO) {
                if (MainActivity.connected) {
                    try {
                        var response =
                            ApiAdapter.retrofit.getTakenAnkete(AccountRepository.getHash())

                        if (response.size == 0)
                            return@withContext null

                        var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                        if (db != null)
                            db!!.anketaTakenDao().insertAll(response)

                        return@withContext response
                    } catch (ex: Exception) {
                        return@withContext null
                    }
                } else {
                    var db = AccountRepository.context?.let { AppDatabase.getInstance(it) }
                    if (db != null) {
                        var ta = db!!.anketaTakenDao().getAll()
                        return@withContext  ta
                    }
                    return@withContext listOf()
                }
            }
        }
    }
}