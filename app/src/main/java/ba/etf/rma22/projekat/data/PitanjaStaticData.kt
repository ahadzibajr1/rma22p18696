package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa

fun svaPitanja() : MutableList<Pitanje> {
    return mutableListOf( Pitanje(0,"Pitanje 1", "Pitanje 1", listOf("Opcija 1", "Opcija 2", "Opcija 3"),
        0
    ),
    Pitanje(1,"Pitanje 2", "Pitanje 2", listOf("Opcija 1", "Opcija 2", "Opcija 3"),0),
        Pitanje(2,"Prvo pitanje", "Kojoj dobnoj skupini pripadate?", listOf("ispod 18", "18-30", "30-50", "50+"),0),
        Pitanje(3,"Drugo pitanje", "Da li ste zaposleni?", listOf("Da", "Ne"),0),
        Pitanje(4,"Treće pitanje", "Koji je Vaš spol?", listOf("Muški", "Ženski"),0),
        Pitanje(5,"P1", "P1", listOf("Odgovor 1", "Odgovor 2"),0),
        Pitanje(6,"P2", "P2", listOf("Odgovor 1", "Odgovor 2"),0),
        Pitanje(7,"A2P1", "A2P1", listOf("Da", "Ne"),0),
        Pitanje(8,"A2P2", "A2P2", listOf("1", "2"),0),
        Pitanje(9,"I0A1P1", "I0A1P1", listOf("1", "2", "3"),0),
        Pitanje(10,"I0A1P2", "I0A1P2", listOf("1", "2"),0),
        Pitanje(11,"I4A3P1", "I4A3P1", listOf("0", "1"),0),
        Pitanje (12,"I4A3P2", "I4A3P2", listOf("da", "ne"),0),
        Pitanje (13,"I4A4P1", "I4A4P1", listOf("1", "2", "3", "4"),0),
        Pitanje(14,"I4A4P2", "I4A4P2", listOf("1", "2", "3", "4", "5"),0),
        Pitanje(15,"I1A3P1", "I1A3P1", listOf("1", "2"),0),
        Pitanje(16,"I1A3P2", "I1A3P2", listOf("1", "2"),0),
        Pitanje(17,"I1A4P1", "I1A4P1", listOf("1", "2"),0),
        Pitanje(18,"I1A4P2", "I1A4P2", listOf("1", "2"),0),
        Pitanje(19,"I1A4P3", "I1A4P3", listOf("1", "2"),0))
}

fun pitanjaPoAnketama() : MutableList<PitanjeAnketa> {
    return mutableListOf(
        PitanjeAnketa(1,0,0,"Pitanje 1", "Anketa 1", "Istraživanje broj 1"),
        PitanjeAnketa(2,0,1,"Pitanje 2", "Anketa 1", "Istraživanje broj 1"),
        PitanjeAnketa(3,1,2,"Prvo pitanje", "Anketa 1", "Istraživanje broj 2"),
        PitanjeAnketa(4,1,3,"Drugo pitanje", "Anketa 1", "Istraživanje broj 2"),
        PitanjeAnketa(5,1,4,"Treće pitanje", "Anketa 1", "Istraživanje broj 2"),
        PitanjeAnketa(6,7,2,"P1", "Anketa 1", "Istraživanje broj 3"),
        PitanjeAnketa(7,2,6,"P2", "Anketa 1", "Istraživanje broj 3"),
        PitanjeAnketa(8,3,7,"A2P1", "Anketa 2", "Istraživanje broj 1"),
        PitanjeAnketa(9,3,8,"A2P2", "Anketa 2", "Istraživanje broj 1"),
        PitanjeAnketa(10,4,9,"I0A1P1", "Anketa 1", "Istraživanje broj 0"),
        PitanjeAnketa(11,4,10,"I0A1P2", "Anketa 1", "Istraživanje broj 0"),
        PitanjeAnketa(12,5,11,"I4A3P1", "Anketa 3", "Istraživanje broj 4"),
        PitanjeAnketa(13,5,12,"I4A3P2", "Anketa 3", "Istraživanje broj 4"),
        PitanjeAnketa(14,6,13,"I4A4P1", "Anketa 4", "Istraživanje broj 4"),
        PitanjeAnketa(15,6,14,"I4A4P2", "Anketa 4", "Istraživanje broj 4"),
        PitanjeAnketa(16,7,15,"I1A3P1", "Anketa 3", "Istraživanje broj 1"),
        PitanjeAnketa(17,7,16,"I1A3P2", "Anketa 3", "Istraživanje broj 1"),
        PitanjeAnketa(18,8,17,"I1A4P1", "Anketa 4", "Istraživanje broj 1"),
        PitanjeAnketa(19,8,18,"I1A4P2", "Anketa 4", "Istraživanje broj 1"),
        PitanjeAnketa(20,8,19,"I1A4P3", "Anketa 4", "Istraživanje broj 1")
    )
}

