package ba.etf.rma22.projekat.view

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import ba.etf.rma22.projekat.R

class MyArrayAdapter(context: Context, @LayoutRes private val layoutResource: Int, private val elements: List<String>):
    ArrayAdapter<String>(context, layoutResource, elements) {
    override fun getView(position: Int, newView: View?, parent: ViewGroup): View {
        var newView = newView
        newView = LayoutInflater.from(context).inflate(R.layout.opcija_item, parent, false)
        val textView = newView.findViewById<TextView>(R.id.tekstOpcije)
        val element = elements.get(position)
        textView.text=element
        /*if(position == odgovor )
            textView.setTextColor(Color.parseColor("#0000FF"))*/



        return newView
    }


}