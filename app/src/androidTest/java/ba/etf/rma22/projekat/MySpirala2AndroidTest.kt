package ba.etf.rma22.projekat

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.anything
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class MySpirala2AndroidTest {
    @get:Rule
    val intentsTestRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun zadatak1Test() {
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
        Espresso.onView(withId(R.id.filterAnketa)).check(
            ViewAssertions.matches(
                isCompletelyDisplayed()
            )
        )
        Espresso.onView(withId(R.id.listaAnketa)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.filterAnketa)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Sve moje ankete"))).perform(click())
        var ankete = AnketaRepository.getMyAnkete()


        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
                CoreMatchers.allOf(
                    ViewMatchers.hasDescendant(withText(ankete[0].naziv)),
                    ViewMatchers.hasDescendant(withText(ankete[0].nazivIstrazivanja))
                )
        ))


        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Sve ankete"))).perform(click())

        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Urađene ankete"))).perform(click())
        ankete = AnketaRepository.getDone()

        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            CoreMatchers.allOf(
                ViewMatchers.hasDescendant(withText(ankete[0].naziv)),
                ViewMatchers.hasDescendant(withText(ankete[0].nazivIstrazivanja))
            )
        ))


        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Buduće ankete"))).perform(click())

        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Prošle ankete"))).perform(click())
        ankete = AnketaRepository.getNotTaken()


        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            CoreMatchers.allOf(
                ViewMatchers.hasDescendant(withText(ankete[0].naziv)),
                ViewMatchers.hasDescendant(withText(ankete[0].nazivIstrazivanja))
            )
        ))

        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToLast())
        onView(withId(R.id.odabirGodina)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("1"))).perform(click())

        var istrazivanja = IstrazivanjeRepository.getIstrazivanjeByGodina(1).filterNot { it in IstrazivanjeRepository.getUpisani() }
        onView(withId(R.id.odabirIstrazivanja)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`(istrazivanja[0].naziv))).perform(click())

        var grupe = GrupaRepository.getGroupsByIstrazivanje(istrazivanja[0].naziv).filterNot { it in GrupaRepository.getUpisaneGrupeOld() }
        onView(withId(R.id.odabirGrupa)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`(grupe[0].naziv))).perform(click())

        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
        onView(withSubstring("Uspješno ste upisani")).check(matches(isDisplayed()))

        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
        Espresso.onView(withId(R.id.filterAnketa)).check(
            ViewAssertions.matches(
                isCompletelyDisplayed()
            )
        )

        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
        Espresso.onView(withId(R.id.odabirGodina)).check(
            ViewAssertions.matches(
                isCompletelyDisplayed()
            )
        )



    }

    @Test
    fun zadatak2Test() {
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
        onView(withId(R.id.filterAnketa)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Sve moje ankete"))).perform(click())
        var ankete = AnketaRepository.getMyAnkete().filter { it.datumRada == null && (it.datumKraj==null || it.datumKraj!!.after(Calendar.getInstance().time)) && it.datumPocetak.before(Calendar.getInstance().time) }

        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(ankete[0].naziv)),
            hasDescendant(withText(ankete[0].nazivIstrazivanja))), click()))

        val pitanja = PitanjeAnketaRepository.getPitanja(ankete[0].naziv, ankete[0].nazivIstrazivanja!!)
        for ((indeks,pitanje) in pitanja.withIndex()) {
            onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(indeks))
            onData(anything())
                .inAdapterView(allOf(withId(R.id.odgovoriLista), isCompletelyDisplayed()))
                .atPosition(0).perform(click());
            if (indeks == pitanja.size-1) {
                onView(withId(R.id.dugmeZaustavi)).perform(click())
            }
        }

        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(ankete[0].naziv)),
            hasDescendant(withText(ankete[0].nazivIstrazivanja))), click()))

        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToLast())
        onView(AllOf.allOf(isDisplayed(), withId(R.id.progresText))).check(matches(withText("100%")))
        onView(withId(R.id.dugmePredaj)).perform(click())

        onView(withSubstring("Završili ste anketu")).check(matches(isDisplayed()))
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
        onView(withId(R.id.filterAnketa)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Urađene ankete"))).perform(click())
        onView(withId(R.id.listaAnketa)).perform(RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(CoreMatchers.allOf(hasDescendant(withText(ankete[0].naziv)),
            hasDescendant(withText(ankete[0].nazivIstrazivanja)))))

    }

    @After
    fun vratiNaDefault() {
        GrupaRepository.vratiNaDefaultStanje()
        IstrazivanjeRepository.vratiNaDefaultStanje()
        AnketaRepository.vratiNaDefault()
        PitanjeAnketaRepository.vratiNaDefault()
    }

}
